tdaq_package()

tdaq_add_executable(scan_manager_rd53a_DCS
                 src/scan_manager_rd53a_DCS.cpp
                 INCLUDE_DIRECTORIES TBB
                 LINK_LIBRARIES itk-is ItkMonitor tdaq::netio tdaq::cmdline tdaq::is TBB pthread RD53A ROOT ROOT::Hist ROOT::RIO)

