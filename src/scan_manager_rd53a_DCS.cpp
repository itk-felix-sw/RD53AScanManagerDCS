#include <RD53Emulator/Handler.h>
#include <RD53Emulator/FastDigitalScan.h>
#include <RD53Emulator/DigitalScan.h>
#include <RD53Emulator/AnalogScan.h>
#include <RD53Emulator/ThresholdScan.h>
#include <RD53Emulator/TestScan.h>
#include <RD53Emulator/SensorScan.h>
#include <RD53Emulator/ToTScan.h>
#include <RD53Emulator/MaskCheck.h>
#include <RD53Emulator/GlobalThresholdTune.h>
#include <RD53Emulator/PreampTune.h>
#include <RD53Emulator/PixelThresholdTune.h>
#include <RD53Emulator/NoiseScan.h>
#include <RD53Emulator/ConfigScan.h>

#include <cmdl/cmdargs.h>
#include <signal.h>
#include <sstream>
#include <chrono>
#include <map>
#include <fstream>
#include <is/info.h>
#include <is/infodictionary.h>
#include <is/infoT.h>
#include <is/inforeceiver.h>
#include <ipc/core.h>
#include <itkdal/ITkResource.h>
#include <itkdal/ITkSPchain.h>
#include <itkdal/ITkModule.h>
#include <itkdal/ITkChip.h>
#include <itk-monitor/ItkMonitor.h>

using namespace std;
using namespace RD53A;

// callback to get the notification on changes on the IS address
// move
void callback(ISCallbackInfo * isc){//, string SPchain){
  std::cout << "CALLBACK:: " << isc->name() << std::endl;
  std::cout << "Reason code : " << isc->reason() << std::endl;
  
  // Getting the new value
  ISInfoString isi;
  isc -> value(isi);
  std::cout << "New value ISSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS: " << isi;
}

int main(int argc, char *argv[]){

  cout << "#####################################" << endl
       << "# Welcome to scan_manager_rd53a     #" << endl
       << "#####################################" << endl;

  CmdArgStr  cScan(     's',"scan","scan","scan name",CmdArg::isREQ);
  CmdArgStrList cConfs( 'c',"config","chips","chip names");
  CmdArgBool cVerbose(  'v',"verbose","turn on verbose mode");
  CmdArgStr  cBackend(  'b',"backend","type","posix,rdma. Default posix");
  CmdArgStr  cInterface('i',"interface","network-card","Default enp24s0f0");
  CmdArgStr  cNetFile(  'n',"netfile","file","Network file. Default network.json");
  CmdArgInt  cCharge(   'q',"charge","electrons","Injection charge");
  CmdArgBool cRetune(   'r',"retune","enable retune mode");
  CmdArgStr  cOutPath(  'o',"outpath","path","output path. Default $ITK_DATA_PATH");
  CmdArgInt  cToT(      't',"ToT","bc","target ToT");
  CmdArgInt  cThreshold('a',"threshold","electrons","threshold target");
  CmdArgInt  cWait(     'w',"wait","seconds","time to wait between config and run. Default: 3");
  CmdArgStr  cFrontEnd( 'f',"frontend","type","sync, lin, diff, all. Some scans accept frontend range");
  CmdArgBool cStoreHits('H',"hits","turn on the store hits flag");
  CmdArgStr  cPartition('p', "partition", "partition-name", "Name of the partition. Default $TDAQ_PARTITION.");
  CmdArgStr  cDCSmapping('d',"dcsMapping","path","name of the file in configDB with the mapping between physical links and DCS elements. Default chipsmapping");
  CmdArgStr  cServer(   'g', "server", "is-server", "IS server");
  
  CmdLine cmdl(*argv,&cScan,&cFrontEnd,&cConfs,&cVerbose,&cBackend,&cInterface,&cNetFile,&cCharge,&cRetune,&cOutPath,&cToT,&cThreshold,&cWait,&cStoreHits,&cPartition,&cDCSmapping,&cServer,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
  std::string commandLineStr="";
  for (int i=1;i<argc;i++) commandLineStr.append(std::string(argv[i]).append(" "));

  string scan(cScan);
  string backend(cBackend.flags()&CmdArg::GIVEN?cBackend:"posix");
  string interface=(cInterface.flags()&CmdArg::GIVEN?cInterface:"enp24s0f0");
  string netfile=(cNetFile.flags()&CmdArg::GIVEN?cNetFile:"connectivity-emu-rd53a.json");
  string DCSmapping=(cDCSmapping.flags()&CmdArg::GIVEN?cDCSmapping:"chipsmapping"); //generated from itk-felix-sw/RD53AConfigs/data/$cDCSMapping.json

  // Initialise communication library
  // IPCCore::init(argc, argv);

  // Create the instance of partition
  string partition((cPartition.flags() & CmdArg::GIVEN?cPartition:getenv("TDAQ_PARTITION")));
  if(cVerbose) cout << "Create IPC partition: " << partition << endl;
  IPCPartition part(partition);

  // Create the IS dictionary instance using the partition object as parameter
  if(cVerbose) cout << "Create IS dictionary" << endl;
  ISInfoDictionary is_server(part);
  string server(cServer);

  Handler * handler = 0;
  if(scan.find("FastDigitalScan")!=string::npos)   {handler=new FastDigitalScan();}
  else if(scan.find("DigitalScan")!=string::npos)  {handler=new DigitalScan();}
  else if(scan.find("Ana")!=string::npos)          {handler=new AnalogScan();}
  else if(scan.find("ThrScan")!=string::npos)      {handler=new ThresholdScan();}
  else if(scan.find("Test")!=string::npos)         {handler=new TestScan();}
  else if(scan.find("Sensor")!=string::npos)       {handler=new SensorScan();}
  else if(scan.find("Config")!=string::npos)       {handler=new ConfigScan();}
  else if(scan.find("ToT")!=string::npos)          {handler=new ToTScan();
    if(!(cCharge.flags()&CmdArg::GIVEN)){cout << "Injection charge required (-q option)." << endl; return 0;
    }
  }
  else if(scan.find("PreampSyn")!=string::npos or 
	  scan.find("PreampLin")!=string::npos or 
	  scan.find("PreampDiff")!=string::npos)   {handler=new PreampTune(); 
      if(!(cToT.flags()&CmdArg::GIVEN)){cout << "ToT target required (-t option)." << endl; return 0;}          
      if(!(cCharge.flags()&CmdArg::GIVEN)){cout << "Injection charge required (-q option)." << endl; return 0;} 
  }
  else if(scan.find("MaskCheck")!=string::npos)    {handler=new MaskCheck();}
  else if(scan.find("Glob")!=string::npos )     {handler=new GlobalThresholdTune();
    if(!(cThreshold.flags()&CmdArg::GIVEN)){cout << "Threshold target required (-a option)." << endl; return 0;}
  }
  else if(scan.find("PixLin")!=string::npos or
	  scan.find("PixDiff")!=string::npos)      {handler=new PixelThresholdTune();
    if(!(cThreshold.flags()&CmdArg::GIVEN)){cout << "Threshold target required (-a option)." << endl; return 0;}
  }
  else if(scan.find("Noise")!=string::npos)        {handler=new NoiseScan();}

  else{
    cout << "Scan Manager: Scan not recognized: " << scan << endl;
    return 0;
  }

  handler->SetVerbose(cVerbose);
  handler->SetContext(backend);
  handler->SetInterface(interface);
  handler->SetMapping(netfile,(cConfs.count()==0));
  handler->SetRetune(cRetune);
  handler->SetScan(scan);
  handler->SetCommandLine(commandLineStr);

  for(uint32_t i=0;i<cConfs.count();i++){
    cout << "Scan Manager: Adding front end #" << i << " " << string(cConfs[i]) << endl;
    handler->AddFE(string(cConfs[i]));
  }

  //  if(cVerbose) cout << "Read the chip position from mapping file";
  //handler -> LoadChipMapping(DCSmapping);

  if(cVerbose) cout << "Read from IS" << endl;
  ItkMonitor * monitor = new ItkMonitor();
  monitor->SetPartition(partition);
  for(auto fe : handler -> GetFEs()){
    string dcs_node = fe -> GetDcsNode();
    string SPchainName = dcs_node.substr(0,dcs_node.find("."));
    string SPchainAddress = server + "." + SPchainName;
    ITkSPchain * chain = monitor -> GetSPchain(SPchainAddress);
    std::string FSMstate = chain -> state;
    std::cout << "Reading FSM state from address " << SPchainAddress << ": " << FSMstate << std::endl;
    std::cout << "Reading LV state from address " << SPchainAddress << ": " << chain -> LV << std::endl;
    std::cout << "Reading HV state from address " << SPchainAddress << ": " << chain -> HV << std::endl;
    std::cout << "Reading Opto state from address " << SPchainAddress << ": " << chain -> Opto << std::endl;
    std::cout << "Reading Tilock state from address " << SPchainAddress << ": " << chain -> Tilock << std::endl;

    //if (FSMstate != "LV_ON" && FSMstate != "ON"){
    if (FSMstate != "ON"){
      std::cout << "FSM not READY! Can't start the scan -> exiting" << std::endl;
      return 0;
    }

    cout << "FSM READY -> proceeding" << endl;
    std::string fullModuleAddress = server + "." + dcs_node;
    monitor -> SetModule(fullModuleAddress, scan);
    ITkModule * module = monitor -> GetModule(fullModuleAddress);
    std::cout << "Writing \"" << module -> runType << "\" on IS address " << fullModuleAddress << ".runType" << std::endl;

  // Subscribing to IS server with info on the whole SPchain
  // Create the IS receiver instance the specific partitition
  ISInfoReceiver rec(partition);
  rec.subscribe(SPchainAddress, callback);

  }

  //std::this_thread::sleep_for(std::chrono::seconds(15));
  
  //return 0;
  cout << "Scan Manager: Configure the scan" << endl;
  if(cOutPath.flags()&CmdArg::GIVEN){
    handler->SetOutPath(string(cOutPath));
  }
  if(cCharge.flags()&CmdArg::GIVEN){
    handler->SetCharge(cCharge);
  }
  if(cToT.flags()&CmdArg::GIVEN){
    handler->SetToT(cToT);
  }
  if(cThreshold.flags()&CmdArg::GIVEN){
    handler->SetThreshold(cThreshold);
  }
  if(cFrontEnd.flags()&CmdArg::GIVEN){
    handler->SetScanFE(string(cFrontEnd));
  }
  if(cStoreHits.flags()&CmdArg::GIVEN){
    handler->StoreHits(cStoreHits);
  }

  cout << "Scan Manager: Connect" << endl;
  /*for(auto fe : handler -> GetFEs()){
    monitor -> SetChip(fe->GetDcsNode(), "Connecting", 0);
  } // repeat this for loop before any call to handler from now on
  */
  handler->Connect();

  cout << "Scan Manager: InitRun" << endl;
  //monitor -> SetChip(chipAddress, "InitRun", 1.1);
  handler->InitRun();

  cout << "Scan Manager: Config" << endl;
  //monitor -> SetChip(chipAddress, "Configuring", 2.2);
  handler->Config();

  cout << "Scan Manager: PreRun" << endl;
  //monitor -> SetChip(chipAddress, "PreRun", 3.3);
  handler->PreRun();
  
  cout << "Scan Manager: Save configuration before the run" << endl;
  //monitor -> SetChip(chipAddress, "Saving config", 4.4);
  handler->SaveConfig("before");

  cout << "Scan Manager: Wait for run to start: " << endl;
  //monitor -> SetChip(chipAddress, "Waiting", 5.5);
  sleep(cWait);

  cout << "Scan Manager: Run" << endl;
  //monitor -> SetChip(chipAddress, "Running", 6.6);
  handler->Run();

  // Sending events rate to DCS //AAA                                                                                                                                                                              
  for(auto fe : handler -> GetFEs()){
    float elapsedTime = handler -> GetElapsedTime(fe -> GetName());
    int totalEventsNumber = fe -> GetTotalEventsNumber();
    float eventsRate = totalEventsNumber / elapsedTime;
    cout << "Total events number: " << totalEventsNumber << " -- elapsedTime: " << elapsedTime << " microseconds --> events rate: " << eventsRate << " in MHz" << endl;
    elapsedTime = handler -> GetElapsedTime(fe -> GetName()) * pow(10, -6); // time in seconds
    eventsRate = totalEventsNumber / elapsedTime;
    cout << "Total events number: " << totalEventsNumber << " -- elapsedTime: " << elapsedTime << " seconds --> events rate: " << eventsRate << " in Hz" << endl;
    string fullChipAddress = fe->GetDcsNode();
    //monitor -> SetChip(fullChipAddress, "End of the scan", eventsRate);
    
  }

  cout << "Scan Manager: Analysis" << endl;
  //monitor -> SetChip(chipAddress, "Analysing", 7.7);
  handler->Analysis();

  cout << "Scan Manager: Disconnect" << endl;
  //monitor -> SetChip(chipAddress, "Disconnecting", 8.8);
  handler->Disconnect();

  cout << "Scan Manager: Save configuration after the run" << endl;
  //monitor -> SetChip(chipAddress, "Saving config", 9.9);
  handler->SaveConfig("after");

  cout << "Scan Manager: Save the tuned files" << endl;
  //monitor -> SetChip(chipAddress, "Saving files", 10.0);
  handler->SaveConfig("tuned");

  cout << "Scan Manager: Save the histograms" << endl;
  //monitor -> SetChip(chipAddress, "Saving histograms", 11.1);
  handler->Save();

  cout << "Scan Manager: Cleaning the house" << endl;
  //monitor -> SetChip(chipAddress, "Cleaning", 12.2);
  delete handler;

  cout << "Scan Manager: Have a nice day" << endl;
  //monitor -> SetChip(chipAddress, "Done", -999);
  return 0;

}
